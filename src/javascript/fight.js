import Fighter from './fighter';
import FightersView from './fightersView';

class Fight  {
    constructor (fighters){
        const array = Array.from(fighters);
        const fighter_1 = array[0][1];
        const fighter_2 = array[1][1];
        this.fight(new Fighter(fighter_1),new Fighter(fighter_2));
    }

    fight (fighter_1 , fighter_2) {

        do {
            this.hit(fighter_1 , fighter_2);
            this.hit(fighter_2 , fighter_1);
        }
        while (fighter_1.health >0 && fighter_2.health > 0);

        const winner = fighter_1.health > fighter_2.health ? fighter_1 : fighter_2;

        this.gameOver(winner);


    }

    hit (fighter , enemy) {
        if(fighter.health > 0){
            const decreaseHealth = Math.max( fighter.getHitPower() - enemy.getBlockPower(),0);
            enemy.health = enemy.health - decreaseHealth;
            console.log(fighter.name + ' hit ' + enemy.name + '; '+ enemy.name + ' health:' +enemy.health);
        }
    }

    gameOver(winner){

        console.log(winner);

        document.getElementById('game-over').innerText = 'Winner '+ winner.name;

        const elements = document.getElementsByClassName('check');
        for (var i = 0; i < elements.length; i++) {
            let elem = elements[i];
            if (elem.classList.contains('active')) {
                elem.removeAttribute('disabled');
                if (elem.checked) {
                    elem.checked = false;
                }
            }

        }

        document.getElementById('start').style.visibility = 'hidden';
        FightersView.arenaMap.clear();
    }
}

export default Fight;