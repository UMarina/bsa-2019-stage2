import View from './view';

class FighterView extends View {
  constructor(fighter, handleClick, handleCheckClick) {
    super();

    this.createFighter(fighter, handleClick ,handleCheckClick);
  }

  createFighter(fighter, handleClick,handleCheckClick) {
    const {_id,  name, source} = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const checkElement = this.createCheck(_id);

    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.append(checkElement,imageElement, nameElement);
    imageElement.addEventListener('click', event => handleClick(event, fighter), false);
    checkElement.addEventListener('click', event => handleCheckClick(event, checkElement, _id), false);
  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }
    createCheck(id) {
    const attributes = { type: 'checkbox', value: id ,disabled : 'disabled',id : 'check_'+id };
    const checkElement = this.createElement({ tagName: 'input', className: 'check', attributes});
        return checkElement;
    }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }
}

export default FighterView;