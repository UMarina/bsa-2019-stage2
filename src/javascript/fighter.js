class Fighter {
    constructor(fighter) {
        const { attack, defense, health, name, source, _id } = fighter;

        this.name = name;
        this.attack = attack;
        this.health = health;
        this.defense = defense;
        this.id = _id;
    }

    getHitPower(){
        const criticalHitChance = Math.random()+1;
        const powerHit = this.attack * criticalHitChance;
        return powerHit;
    }

    getBlockPower() {
        const dodgeChance = Math.random()+1;
        const powerBlock = this.defense * dodgeChance;
        return powerBlock;
    }
}

export default Fighter;
