import App from './app';
import View from './view';
import Fight from './fight';
import FighterView from './fighterView';
import {fighterService} from "./services/fightersService";

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.handleChClick = this.handleCheckClick.bind(this);
    this.createFighters(fighters);

      document.getElementById('modal-confirm').addEventListener('click', event => this.handleModalClick(), false);
      document.getElementById('close').addEventListener('click', event => this.closeModal(), false);
  }

    fightersDetailsMap = new Map();
    static modalBlock = document.getElementById('modal-block');
    static currentFighterId;
    static arenaMap = new Map();


  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick,this.handleChClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
      const { _id } = fighter;
      FightersView.currentFighterId = _id;

      if(!this.fightersDetailsMap.has(_id)) {
          const fighterDetails = await fighterService.getFighterDetails(_id);
          this.fightersDetailsMap.set(_id, fighterDetails);
      }

      this.createForm(this.fightersDetailsMap.get(_id), event.clientX, event.clientY);
      if (FightersView.arenaMap.size < 2){
          document.getElementById('check_'+_id).removeAttribute('disabled');
      }
      document.getElementById('check_'+_id).classList.add('active');
  }

    createForm(fighter,x,y) {

        const { attack, defense, health, name, source, _id } = fighter;
        this.addDefense(defense);
        this.addAttack(attack);
        this.addHealth(health);
        this.addName(name);

        this.openModal(x,y);
    }

    addDefense(defense) {
        const defenseElement = document.getElementById('defense');
        defenseElement.value = defense;
    }
    addAttack(attack) {
        const attackElement = document.getElementById('attack');
        attackElement.value = attack;
    }
    addHealth(health) {
        const healthElement = document.getElementById('health');
        healthElement.value = health;
    }
    addName(name) {
        const nameElement = document.getElementById('form-name');
        nameElement.innerText = name;
    }

    handleModalClick(){
      if(FightersView.currentFighterId > 0) {
          const health = document.getElementById('health').value;
          const attack = document.getElementById('attack').value;
          const defense = document.getElementById('defense').value;
          const oldFighter = this.fightersDetailsMap.get(FightersView.currentFighterId);
          oldFighter.health = +health;
          oldFighter.attack = +attack;
          oldFighter.defense = +defense;


          this.fightersDetailsMap.set(oldFighter._id, oldFighter);
          this.closeModal();
          FightersView.currentFighterId = 0;
      }

    }
    closeModal (){
        FightersView.modalBlock.style.visibility = 'hidden';
    }
    openModal (x,y){
      const left = x - 75;
      const top = y - 90;
        FightersView.modalBlock.style.left = left + 'px';
        FightersView.modalBlock.style.top = top + 'px';
        FightersView.modalBlock.style.visibility = 'visible';
    }

    handleCheckClick(event, el, _id) {
        if (el.checked) {
            FightersView.arenaMap.set(_id, this.fightersDetailsMap.get(_id));
        } else {
            FightersView.arenaMap.delete(_id);
        }
        const elements = document.getElementsByClassName('check');
            for (var i = 0; i < elements.length; i++) {
                let elem = elements[i];
                if (FightersView.arenaMap.size == 2) {
                    if (!elem.checked) {
                        elem.setAttribute('disabled', 'disabled');
                    }
                } else {
                    if (elem.classList.contains('active')) {
                        elem.removeAttribute('disabled');
                    }
                }
            }
        if (FightersView.arenaMap.size == 2) {
                if(document.getElementById('start') == null){
                    this.addStartButton();
                } else {
                    document.getElementById('start').style.visibility = 'visible';
                }
        } else {
            if(document.getElementById('start') != null) {
                document.getElementById('start').style.visibility = 'hidden';
            }
        }
        document.getElementById('modal-block').style.visibility = 'hidden';

    }
    addStartButton(){
      const div = this.createElement({ tagName: 'div', className: 'button-block' });
      const attributes = {id : 'start'}
      const button = this.createElement({ tagName: 'button', className: 'start-button', attributes });
      button.innerText = "START";
      div.append(button);
      button.addEventListener('click', event => this.startFight(), false);
      App.rootElement.append(div);
    }

    startFight(){

        new Fight(FightersView.arenaMap);
    }


}

export default FightersView;